# Coding Demos

These are some coding samples or small apps I've built.

| Link to app/demo | Blog? | What is it? |
| ---- | ---- | ---- |
| [React Note App](https://react-noteapp.surge.sh) |  [React Noteapp](/blog/react-noteapp) |Note taking app built with **ReactJS** that saves to browser's local storage|
| [React Timesheet App](https://timesheet.thinkingk.app) | [Dime for Time](/blog/dime-for-time) |A simple timesheet keeping app built with **ReactJS** and **Google Firebase** in the back-end.|
