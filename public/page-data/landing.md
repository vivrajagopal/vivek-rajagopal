# Vivek Rajagopal

<img src="avatar.svg" style="height: 8rem; margin: 1.5rem 0" />

I like building interesting products and tools with code. I like working on all aspects from front-end, to back-end infrastructure and dev-ops. My favourite languages are `TypeScript` and `F#`.

I like to keep busy by trying out new frameworks and languages.

I love Electronic music / EDM, and immersive-sim style games like Dishonored and Deus Ex. I am a huge fan of simulation games like [Factorio](https://www.youtube.com/watch?v=5yLaYuuBWRM) and SimCity. I also partake in the occasional hike and gym sesh.

Check out my <internal route="/work">work</internal>

<span class="links" style="text-align:center;">
[![](https://image.flaticon.com/icons/svg/25/25231.svg)](https://github.com/VivekRajagopal)
[![](https://image.flaticon.com/icons/svg/34/34405.svg)](https://www.linkedin.com/in/vivek-rajagopal-0a60ab9b/)
</span>
