# Vivek Rajagopal: Personal Website

[Visit](https://vivekrajagopal.dev)

The site is built with ReactJS. The content is written in markdown, and dynamically fetched and rendered depending on the route.
