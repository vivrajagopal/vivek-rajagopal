import React from "react";

import "./page-styles/Landing.scss";
import "./page-styles/CV.css";
import "./page-styles/DriveClear.css";
import "./page-styles/About.css";
import "./page-styles/Blog.css";

import "./FontIcon.css";

import PagesData from "./page-data";
import RenderedMarkdown from "./RenderedMarkdown";

const RenderedPage = (path: string, classes: string[]) => (
  <RenderedMarkdown path={path} className={classes.join(" ")} />
);

const routes = PagesData.map((pageData) => {
  return {
    path: pageData.route,
    navHide: !pageData.navLink,
    exact: true,
    name: pageData.name,
    icon: pageData.navIcon,
    component: () => RenderedPage(pageData.contentPath, pageData.classes || ["content"])
  };
});

export default routes;
