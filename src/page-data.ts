import BasePagesData from "./page-data/base-pages.json";
import BlogPagesData from "./page-data/blog-pages.json";

const errorPage = "/page-data/error.md";

type PageDto = {
  name: string;
  datePublished: string;
  route: string;
  contentPath: string;
  navLink?: boolean;
  navIcon?: string;
  classes?: string[];
};

type Page = {
  name: string;
  datePublished: Date;
  route: string;
  contentPath: string;
  navLink?: boolean;
  navIcon?: string;
  classes?: string[];
};

const dtoToPage = (dto: PageDto): Page => ({
  ...dto,
  datePublished: new Date(dto.datePublished)
});

const BasePages: Page[] = (BasePagesData as PageDto[]).map(dtoToPage);
const BlogPages: Page[] = (BlogPagesData as PageDto[]).map(dtoToPage);

const UnlinkedPages: Page[] = [
  {
    name: "Drive Clear",
    datePublished: new Date(2019, 6, 2),
    route: "/driveclear",
    contentPath: "/page-data/driveclear.md",
    classes: ["driveclear", "content"]
  }
];

const PagesData = [...BasePages, ...BlogPages, ...UnlinkedPages];

const doesPageExist = (pageContentPath: string) => {
  return PagesData.find((page) => page.contentPath === pageContentPath) !== undefined;
};

const fetchPageText = (pageContentPath: string) => {
  if (doesPageExist(pageContentPath)) {
    return fetch(pageContentPath).then((res) => res.text());
  } else {
    return fetch(errorPage).then((res) => res.text());
  }
};

export default PagesData;
export { doesPageExist, fetchPageText };
