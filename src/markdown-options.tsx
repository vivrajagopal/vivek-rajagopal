import React from "react";
import { NavLink } from "react-router-dom";

import { doesPageExist } from "./page-data";

const InternalRoute = (element: HTMLElement & { route: string }) => {
  const { route } = element;
  return (
    <NavLink exact to={route}>
      <span>{element.children[0]}</span>
    </NavLink>
  );
};

const FormatRouteLink = (anchorElem: HTMLAnchorElement) => {
  const name = anchorElem.children[0];
  const href = anchorElem.href;
  const pageName = `/page-data${href}.md`;

  return doesPageExist(pageName) ? (
    <NavLink key={href} exact to={href}>
      <span> {name} </span>
    </NavLink>
  ) : (
    <a href={href} target="_blank" rel="noopener noreferrer">
      {name}
    </a>
  );
};

export const formatRouteLinkOverride = {
  a: {
    component: FormatRouteLink
  },
  internal: {
    component: InternalRoute
  }
};
