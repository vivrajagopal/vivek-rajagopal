import { RouteProps } from "react-router";
import { NavLink } from "react-router-dom";
import React from "react";

export const NotFoundPage = ({ location }: RouteProps) => (
  <div className="landing fade-in">
    <h2>Whoops... ¯\_(ツ)_/¯</h2>
    <p>
      Couldn't find <code>{location && location.pathname}</code>
    </p>
    <NavLink key={"/"} exact activeClassName="navbar-link-active" to={"/"}>
      <span className="">Try going home?</span>
    </NavLink>
  </div>
);
