import React from "react";
import { BrowserRouter as Router, NavLink, Route, Switch } from "react-router-dom";
import Analytics from "react-router-ga";
import { DevMode } from "./app-config";
import "./App.scss";
import "./FontIcon.css";
import routes from "./routes";
import { NotFoundPage } from "./NotFoundPage";

const Navbar = () => (
  <div className="navbar-container">
    {routes
      .filter((route) => !route.navHide)
      .map((route) => (
        <NavLink key={route.path} exact className="navbar-link" activeClassName="navbar-link active" to={route.path}>
          <i className={`font-icon ${route.icon}`} />
          <h4 className="navbar-link-name"> {route.name}</h4>
        </NavLink>
      ))}
  </div>
);

const Routes = () => routes.map((route) => <Route key={route.path} {...route} />);

class App extends React.Component {
  componentDidMount = () => {
    window.document.title = "Vivek Rajagopal";
  };

  render = () => (
    <Router>
      <Analytics id={DevMode ? "dummy-id" : "UA-137346231-2"} debug={DevMode}>
        <div className="router-body">
          {Navbar()}
          <Switch>
            {Routes()}
            <Route component={NotFoundPage} />
          </Switch>
        </div>
      </Analytics>
    </Router>
  );
}

export default App;
