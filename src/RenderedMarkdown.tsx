import React, { Component } from "react";
import Markdown from "markdown-to-jsx";

import { fetchPageText } from "./page-data";

import { formatRouteLinkOverride } from "./markdown-options";

type RenderedMarkdownProps = {
  markdownOverrides?: any[];
  path: string;
  className: string;
};

type RenderedMarkdownState = { content: string };

class RenderedMarkdown extends Component<RenderedMarkdownProps, RenderedMarkdownState> {
  private markdownOverrides: any[];

  constructor(props: RenderedMarkdownProps) {
    super(props);
    this.markdownOverrides = this.props.markdownOverrides ? this.props.markdownOverrides : [];
    this.markdownOverrides = [...this.markdownOverrides, formatRouteLinkOverride];
  }

  public state = {
    content: ""
  };

  componentDidMount() {
    fetchPageText(this.props.path).then((content) => this.setState({ content }));
  }

  render() {
    return (
      <Markdown
        className={`${this.props.className} ${this.state.content ? "fade-in" : ""}`}
        options={{ overrides: Object.assign({}, ...this.markdownOverrides) }}
      >
        {this.state.content}
      </Markdown>
    );
  }
}

export default RenderedMarkdown;
