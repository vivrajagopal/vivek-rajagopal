declare module "react-router-ga" {
  interface AnalyticsProps {
    id: string;
    debug: boolean;
  }

  class Analytics<
    T extends AnalyticsProps = AnalyticsProps
  > extends React.Component<T, any> {}

  export default Analytics;
}
